import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Products from './pages/Products/Products';
import Home from './pages/Home/Home';
import Product from './pages/Product/Product';
import Login from './pages/Login/Login';
import './assets/Global.scss';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Cart from './components/Cart/Cart';
import { useState } from 'react';
import Search from './pages/SearchBtn/Search/Search';

function App() {
    const [cartIsShown, SetCartIsShown] = useState(false);
    const showCart = () => {
        SetCartIsShown(true);
    };
    const hideCart = () => {
        SetCartIsShown(false);
    };
    return (
        <div className="App">
            <Header showCartBtn={showCart} />
            <Routes>
                <Route path="/products/:id" element={<Product />} />
                <Route path="/search" element={<Search />} />
                <Route path="/" element={<Home />} />
                <Route path="/products" element={<Products />} />
                <Route path="/login" element={<Login />} />
                {cartIsShown && (
                    <Route path="/cart" element={<Cart onClose={hideCart} />} />
                )}
            </Routes>
            <Footer />
        </div>
    );
}

export default App;
